import 'package:Receitas/data/dummy_data.dart';
import 'package:Receitas/models/settings.dart';
import 'package:Receitas/screens/meal_detail_screen.dart';
import 'package:Receitas/screens/settings_screen.dart';
import 'package:Receitas/screens/tabs_screen.dart';
import 'package:flutter/material.dart';
import 'models/meal.dart';
import 'screens/categories_screen.dart';
import 'screens/categories_meals_screen.dart';
import 'utils/app_route.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<Meal> _availableMeals = DUMMY_MEALS;
  List<Meal> _favoriteMeals = [];
  Settings settings = Settings();

  void _filterMeals(Settings settings) {
    setState(() {
      this.settings = settings;
      _availableMeals = DUMMY_MEALS.where((meal) {
        final filterGluten = settings.isGlutenFree && !meal.isGlutenFree;
        final filterLactose = settings.isLactoseFree && !meal.isLactoseFree;
        final filterVegan = settings.isVegan && !meal.isVegan;
        final filterVegetarian = settings.isVegetarian && !meal.isVegetarian;

        return !filterGluten &&
            !filterLactose &&
            !filterVegan &&
            !filterVegetarian;
      }).toList();
    });
  }

  void _toggleFavorite(Meal meal) {
    setState(() {
      _favoriteMeals.contains(meal)
          ? _favoriteMeals.remove(meal)
          : _favoriteMeals.add(meal);
    });
  }

  bool _isFavorite(Meal meal) {
    return _favoriteMeals.contains(meal);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'DeliMeals',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
        accentColor: Colors.amber,
        fontFamily: 'Raleway',
        canvasColor: Color.fromRGBO(255, 254, 229, 1),
        textTheme: ThemeData.light().textTheme.copyWith(
              headline6: TextStyle(
                fontSize: 20,
                fontFamily: 'RobotoCondensed',
              ),
            ),
      ),
      //Rotas são por prioridade
      routes: {
        //primeira
        AppRouts.HOME: (ctx) => TabsScreen(_favoriteMeals),
        AppRouts.CATEGORIES_MEALS: (ctx) =>
            CategoriesMealsScreen(_availableMeals),
        AppRouts.MEAL_DETAIL: (ctx) =>
            MealDetailScreen(_toggleFavorite, _isFavorite),
        AppRouts.SETTINGS: (ctx) => SettingsScreen(settings, _filterMeals)
      },
      onGenerateRoute: (settings) {
        //segunda - rotas dinamicas
        if (settings.name == '/alguma-coisa') {
          return null;
        } else if (settings.name == '/nao-encontrei') {
          return null;
        } else {
          return MaterialPageRoute(builder: (_) {
            return CategoriesScreen();
          });
        }
      },
      onUnknownRoute: (settings) {
        //terceira - rota desconhecida
        return MaterialPageRoute(builder: (_) {
          return CategoriesScreen();
        });
      },
    );
  }
}
